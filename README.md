## Quick Start

1. Instal python, pip and virtualenv

2. Clone this repo
```
git clone ...
cd ...
```

3. Create virtual env
```
python -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

4. Execute Application

```
FLASK_ENV=development python app.py
```

