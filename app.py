from flask import Flask
from os import environ

app = Flask(__name__)


@app.route('/')
def index() -> str:
    return "Hello World!!!"


if __name__ == '__main__':
    port = int(environ.get("PORT", 5000))
    app.run(host='0.0.0.0',port=port,debug=True)
